from django.contrib import admin
from core import models


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    """Список стран"""
    list_display = ('name',)


@admin.register(models.Region)
class RegionAdmin(admin.ModelAdmin):
    """Список регионов"""
    list_display = ('name', 'country')
    list_filter = ('country',)


@admin.register(models.Area)
class AreaAdmin(admin.ModelAdmin):
    """Административная область"""
    list_display = ('name', 'number', 'region')
    list_filter = ('region',)


@admin.register(models.Brand)
class BrandAdmin(admin.ModelAdmin):
    """Марка качества, определённая географичнски"""
    # change_form_template = 'admin/my_change_form.html'
    list_display = ('name', 'country', 'region', 'area')
    fieldsets = (
        ('Данные', {
            'fields': ('name',)
        }),
        ('Географическая привязка', {
            'fields': ('country', 'region', 'area')
        }),
    )

    class Media:
        js = ("core/js/admin.js",)


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    """Продукция"""
    list_display = ('name', 'brand')
    fieldsets = (
        ('Данные', {
            'fields': ('name', ('brand', 'letter'))
        }),
        ('Географическая привязка', {
            'fields': ('country', 'region', 'area')
        }),
    )

    class Media:
        js = ("core/js/admin.js",)

import string
from django.db import models


LETTERS = []
for i in string.ascii_uppercase:
    LETTERS.append((i, i))


class Country(models.Model):
    """Список стран"""

    name = models.CharField(max_length=100, verbose_name='Страна')

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Список стран'
        ordering = ('name',)

    def __str__(self):
        return self.name


class Region(models.Model):
    """Список регионов"""

    name = models.CharField(max_length=100, verbose_name='Регион')
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Страна')

    class Meta:

        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'
        ordering = ('name', 'country')

    def __str__(self):
        return self.name


class Area(models.Model):
    """Административная область"""

    name = models.CharField(max_length=100, verbose_name='Область')
    number = models.PositiveSmallIntegerField(
        null=True, blank=True, verbose_name='Номер обл.')
    region = models.ForeignKey(
        Region, on_delete=models.CASCADE, verbose_name='Регион')

    class Meta:

        verbose_name = 'Административную область'
        verbose_name_plural = 'Административные области'
        ordering = ('name', 'region')

    def __str__(self):
        return self.name


class Brand(models.Model):
    """Марка качества, определённая географичнски"""

    name = models.CharField(max_length=100, verbose_name='Бренд')
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Страна')
    region = models.ForeignKey(
        Region, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Регион')
    area = models.ForeignKey(
        Area, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Область')

    class Meta:

        verbose_name = 'Марку качества'
        verbose_name_plural = 'Марки качества'
        ordering = ('name', 'country', 'region', 'area')

    def __str__(self):
        return self.name


class Product(models.Model):
    """Продукция"""

    name = models.CharField(max_length=200, verbose_name='Продукт')
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, verbose_name='Страна')
    region = models.ForeignKey(
        Region, on_delete=models.CASCADE, verbose_name='Регион')
    area = models.ForeignKey(
        Area, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Область')
    brand = models.ForeignKey(
        Brand, on_delete=models.CASCADE, verbose_name='Бренд')
    letter = models.CharField(
        max_length=1, choices=LETTERS, blank=True, null=True, verbose_name='Буква')

    class Meta:

        verbose_name = 'Продукцию'
        verbose_name_plural = 'Продукции'
        ordering = ('name',)

    def __str__(self):
        return self.name

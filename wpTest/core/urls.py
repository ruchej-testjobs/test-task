from django.urls import path
from . import views, api


urlpatterns = [
    path('api/get/regions/<int:country_id>',
         api.get_regions, name='get_regions_all'),
    path('api/get/regions/', api.get_regions, name='get_regions'),
    path('api/get/areas/<int:region_id>', api.get_areas, name='get_areas'),
    path('api/get/areas/', api.get_areas, name='get_areas_all'),
    path('api/get/branddata/<int:brand_id>',
         api.get_brand_data, name='get_brand_data'),
    path('api/get/brands/', api.get_brands, name='get_brands'),
]

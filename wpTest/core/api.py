import json
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core import serializers
from core import models


@login_required
def get_regions(request, country_id=0):
    """API возвращает список регионов, принадлежащих выбранной стране"""

    if request.method == 'GET':
        if country_id > 0:
            regions = models.Region.objects.filter(country__pk=country_id)
        else:
            regions = models.Region.objects.all()
        data = serializers.serialize("json", regions)
        data = json.loads(data)
        return JsonResponse(data, safe=False, json_dumps_params={'ensure_ascii': False})


@login_required
def get_areas(request, region_id=0):
    """API возвращает список областей, принадлежащих региону"""

    if request.method == 'GET':
        if region_id > 0:
            areas = models.Area.objects.filter(region__pk=region_id)
        else:
            return JsonResponse([], safe=False)
        data = serializers.serialize("json", areas)
        data = json.loads(data)
        return JsonResponse(data, safe=False, json_dumps_params={'ensure_ascii': False})


@login_required
def get_brand_data(request, brand_id):
    """API возвращает информацию о марке"""

    if request.method == 'GET':
        brand = models.Brand.objects.filter(pk=brand_id)
        data = serializers.serialize("json", brand)
        data = json.loads(data)
        return JsonResponse(data, safe=False, json_dumps_params={'ensure_ascii': False})


@login_required
def get_brands(request):
    """API возвращает список брендов, привязанных географически"""

    if request.method == 'GET':
        country_id = request.GET['country_id']
        region_id = request.GET['region_id']
        area_id = request.GET['area_id']
        pre_filter = {
            'country__pk': country_id,
            'region__pk': region_id,
            'area__pk': area_id,
        }
        filter = {k: v for k, v in pre_filter.items() if v}
        obj = models.Brand.objects.filter(**filter)
        data = serializers.serialize("json", obj)
        data = json.loads(data)
        return JsonResponse(data, safe=False, json_dumps_params={'ensure_ascii': False})

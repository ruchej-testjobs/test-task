'use strict'

let URL_GET_REGIONS = '/api/get/regions'
let URL_GET_AREAS = '/api/get/areas'
let URL_GET_BRAND_DATA = '/api/get/branddata'
let URL_GET_BRANDS = '/api/get/brands'
let selCountry
let selRegion
let selArea
let selBrand

document.addEventListener('DOMContentLoaded', ready)

/**
 * Стартовая функция. Запускает всё после загрузки документа.
 */
function ready() {
  let body = document.body
  if (
    (body.classList.contains('model-brand') ||
      body.classList.contains('model-product')) &&
    body.classList.contains('change-form')
  ) {
    formEditGeoData()
  }
}

/**
 * Получает данные с сервера
 * @params {string} url - Адрес API запроса
 * @return {json} Данные в виде [{"model": "core.region", "pk": 7, "fields": {...}, ]
 */
async function getData(url) {
  let response = await fetch(url)
  if (response.status == 200) {
    let data = await response.json()
    return data
  } else return null
}

// Заполнение набора списка объекта select
function FillOptSel(objSel, data) {
  let currentVal = objSel.value
  objSel.options.length = 1
  for (let i of data) {
    let newOption = new Option(i.fields.name, i.pk)
    objSel.append(newOption)
  }
  let checkOption = [...objSel.options].some(
    (option) => option.value === currentVal
  )
  if (checkOption) {
    objSel.value = currentVal
  }
}

/**
 * Делает замену списка для регионов
 */
async function changeSelectRegion() {
  let country_id = parseInt(selCountry.value)
  if (isNaN(country_id)) country_id = ''
  let regionData = await getData([URL_GET_REGIONS, country_id].join('/'))
  FillOptSel(selRegion, regionData)
  changeSelectArea()
}

/**
 * Делает замену списка для областей
 */
async function changeSelectArea() {
  let id_region = parseInt(selRegion.value)
  if (isNaN(id_region)) id_region = ''
  let areaData = await getData([URL_GET_AREAS, id_region].join('/'))
  FillOptSel(selArea, areaData)
  if (selBrand) changeSelectBrand()
}

/**
 * При выборе в поле бренда, заполняет поля геоданных из
 * информации бренда
 */
async function changeBrand() {
  let id_brand = parseInt(selBrand.value)
  if (isNaN(id_brand)) id_brand = ''
  let brandData = await getData([URL_GET_BRAND_DATA, id_brand].join('/'))
  let countryId = brandData[0].fields.country
  let regionId = brandData[0].fields.region
  let areaId = brandData[0].fields.area

  selCountry.value = countryId ? countryId : ''
  await changeSelectRegion()
  selRegion.value = regionId ? regionId : ''
  await changeSelectArea()
  selArea.value = areaId ? areaId : ''
}

/**
 * Меняем список брендов в зависимости от гео данных
 */
async function changeSelectBrand() {
  let countryId = selCountry.value
  let regionId = selRegion.value
  let areaId = selArea.value
  let brands = await getData(
    [
      URL_GET_BRANDS,
      `?country_id=${countryId}&region_id=${regionId}&area_id=${areaId}`,
    ].join('/')
  )
  FillOptSel(selBrand, brands)
}

/**
 * Обработка форм редактирования связанных геоданных
 */
function formEditGeoData() {
  selCountry = document.getElementById('id_country')
  selRegion = document.getElementById('id_region')
  selArea = document.getElementById('id_area')
  selBrand = document.getElementById('id_brand')
  changeSelectRegion()
  if (selBrand) {
    selBrand.addEventListener('change', changeBrand)
  }
  selCountry.addEventListener('change', changeSelectRegion)
  selRegion.addEventListener('change', changeSelectArea)
}
